<form action="{{ url("/reviews") }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="product_id" value="{{ $product->id }}">
    <label class="block col-md-6">
        <textarea class="form-control" name="text" placeholder="Enter your review" value=""></textarea>
    </label>
    <div class="text-right">
        <button class="btn btn-primary btn-sm">Save Review</button>
    </div>
</form>