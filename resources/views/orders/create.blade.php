@extends('layouts.app')

@section('content')
<div class="container mt-6">
    <h1 class="mb-6">Buy Product</h1>
    <div class="row">
        <div class="col-md-12">
            <form action="{{ url("/order") }}" method="POST">
                {{ csrf_field() }}
                <table>
                    <thead>
                        <th>
                            <td><input type="hidden" name="product_id" value="{{ $product->id }}" /></td>
                            <td>Title</td>
                            <td>Description</td>
                            <td>Price</td>
                            <td>Payment method</td>
                            <td></td>
                        </th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <img src="{{ $product->imageUrl() }}" class="img-fit">
                            </td>
                            <td>{{ $product->title }}</td>
                            <td>{{ $product->description }}</td>
                            <td>{{ $product->price }}</td>
                            <td>
                                <select name="payment_system">
                                    @foreach($paymentSystems as $payment)
                                        <option value="{{ $payment->id }}">{{ $payment->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-primary btn-sm">Buy</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
@endsection
