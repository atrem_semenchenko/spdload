<div>
    @if(!$review->like)
        <form action="{{ url("/reviews/{$review->id}/like") }}" method="POST">
            {{ csrf_field() }}
            <label for="like-{{ $review->id }}" class="btn">Like</label>
            <input id="like-{{ $review->id }}" class="hidden" name="like" type="radio" value="1" onchange="submit()" />
            <label for="dislike-{{ $review->id }}" class="btn">Dislike</label>
            <input id="dislike-{{ $review->id }}" class="hidden" name="like" type="radio" value="0" onchange="submit()" />
        </form>
    @else
        <form action="{{ url("/reviews/like/{$review->like->id}") }}" method="POST" class="d-inline-block">
            {{ csrf_field() }}
            {{ method_field($review->like->like ? 'DELETE' : 'PATCH') }}
            <label for="like-{{ $review->id }}" class="btn {{ $review->like->like ? 'active' : '' }}">Like</label>
            <input id="like-{{ $review->id }}" class="hidden" type="submit" />
        </form>
        <form action="{{ url("/reviews/like/{$review->like->id}") }}" method="POST" class="d-inline-block">
            {{ csrf_field() }}
            {{ method_field($review->like->like ? 'PATCH' : 'DELETE') }}
            <label for="dislike-{{ $review->id }}" class="btn {{ $review->like->like ? '' : 'active' }}">Dislike</label>
            <input id="dislike-{{ $review->id }}" class="hidden" type="submit" />
        </form>
    @endif
</div>