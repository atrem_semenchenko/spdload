@extends('layouts.app')

@section('content')
    <div class="bg-circuit mb-6 border-t border-b border-dark-softer">
        <div class="container">
            <div class="py-9">
                <h1 class="text-3xl">Products</h1>
                <p class="text-2xl text-thin text-dark-soft">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
            @if(Auth::user())
            <div class="py-3">
                <a href="{{ url('/products/new') }}" class="btn-primary btn-sm">Create NEW</a>
            </div>
            @endif
        </div>
    </div>
    <div class="container">
        <div>
            <h1 class="text-bold mb-4">Popular Shows</h1>
            <div class="row pull-x-4 pull-b-6">
                @foreach ($products as $product)
                    <div class="col-2 px-4 mb-6">
                        <div class="hover-grow">
                            <a href="{{ url("/products/{$product->id}") }}" class="block box-shadow mb-2">
                                <img src="{{ $product->imageUrl() }}" class="img-fit">
                            </a>
                            <p class="text-ellipsis">
                                <a href="{{ url("/products/{$product->id}") }}" class="text-sm text-medium">
                                    {{ $product->title }}
                                </a>
                            </p>
                            <p class="text-xs text-uppercase text-spaced text-ellipsis">
                                {{ $product->price }} ₴
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
