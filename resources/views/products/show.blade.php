@extends('layouts.app')

@section('content')
    <div class="container mt-6">
        <div class="row pull-x-4">
            <div class="col-3 px-4">
                <div class="block box-shadow mb-4">
                    <a href="{{ url("/products/{$product->id}") }}">
                        <img src="{{ $product->imageUrl() }}" class="img-fit">
                    </a>
                </div>
            </div>
            <div class="col-9 px-4">
                <div class="mb-6">
                    <div class="flex-spaced flex-y-center mb-4">
                        <div>
                            <h1>
                                <a href="{{ url("/products/{$product->id}") }}" class="text-bold">{{ $product->title }}</a>
                            </h1>
                        </div>
                        <div class="">
                            @if ($product->isOwnedBy(Auth::user()))
                                <a href="{{ url("/products/{$product->id}/edit") }}" class="btn btn-sm btn-secondary">
                                    Edit
                                </a>

                                <form action="{{ url("/products/{$product->id}") }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <input class="btn btn-sm btn-secondary" type="submit" value="Delete" />
                                </form>
                            @endif
                            @if(Auth::user())
                                @include('wishlist.form', ['product' => $product])
                            @endif
                        </div>
                    </div>
                    <div class="mb-4">
                        <p class="text-xs text-uppercase text-spaced text-ellipsis">
                            Price: {{ $product->price }} ₴
                        </p>
                    </div>
                    <div class="mb-4">
                        <p>Description:</p>
                        <p class="text-dark-soft">{{ $product->description }}</p>
                    </div>
                    <a href="{{ url("/order/{$product->id}") }}" class="btn btn-primary">Buy NOW</a>
                </div>
                @if(Auth::user())
                <div>
                    <div class="flex flex-y-baseline mb-4">
                        <h2 class="text-lg mr-4">New review</h2>
                    </div>
                    <div>
                        @include('reviews.create', ['product' => $product])
                    </div>
                </div>
                @endif
                @if (count($reviews) > 0)
                    <div>
                        <div class="flex flex-y-baseline mb-4">
                            <h2 class="text-lg mr-4">Last Reviews</h2>
                            <a href="{{ url("/products/{$product->id}/reviews") }}" class="link-brand">View all</a>
                        </div>
                        <div class="text-sm">
                            @foreach ($reviews->sortDesc() as $review)
                                <div class="border-t flex flex-y-baseline">
                                    <div>
                                        {{ $review->text }}
                                    </div>
                                    <div>
                                        {{ $review->user->name }}
                                    </div>
                                    <div>
                                        {{ $review->created_at->format('M j, Y') }}
                                    </div>
                                </div>
                                @if(Auth::user())
                                <div>
                                    @include("review-likes.form", ['review' => $review])
                                </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                @else
                    <div class="text-center py-6 text-dark-soft text-lg">
                        This product hasn't reviews.
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
