@extends('layouts.app')

@section('content')
<div class="container mt-6">
    <h1 class="mb-6">Product Settings</h1>
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-lg mb-2">Product Details</h2>
            <p class="text-dark-soft">Update your product title, description, and price.</p>
        </div>
        <div class="col-md-12">
            <form action="{{ url("/products/{$product->id}") }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <label class="block col-md-6">
                    <span class="block text-medium mb-2">Title</span>
                    <input class="form-control" name="title" placeholder="The World's Best product" value="{{ old('title', $product->title) }}">
                </label>
                <label class="block col-md-6">
                    <span class="block text-medium mb-2">Description</span>
                    <textarea rows="3" class="form-control" name="description" placeholder="The best product for getting the best information about the best stuff.">{{ old('description', $product->description) }}</textarea>
                </label>
                <label class="block col-md-6">
                    <span class="block text-medium mb-2">Price</span>
                    <input class="form-control" type="number" name="price" placeholder="5000" value="{{ old('price', $product->price) }}">
                </label>
                <label class="block col-md-6">
                    <span class="block text-medium mb-2">Active</span>
                    <input class="form-control" type="radio" name="active" value="1" {{ !$product->active ?: 'checked' }}>Yes
                    <input class="form-control" type="radio" name="active" value="0" {{ $product->active ?: 'checked' }}>No
                </label>
                <div class="text-right">
                    <button class="btn btn-primary btn-sm">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row pull-x-4">
        <div class="col-4 px-4">
            <h2 class="text-lg mb-2">Cover Image</h2>
            <p class="text-dark-soft">Add a new cover image to your product.</p>
        </div>
        <div class="col-8 px-4">
            <form action="{{ url("/products/{$product->id}/cover-image") }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="row pull-x-4">
                    <div class="col-8 px-4 flex-y-end">
                        <div class="flex-fill">
                            <div class="mb-8 flex-center">
                                <input name="cover_path" type="file" class="form-control-file">
                            </div>
                            <div class="text-right">
                                <button class="btn btn-sm btn-primary">Save Image</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
