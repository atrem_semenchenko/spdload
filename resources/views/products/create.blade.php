@extends('layouts.app')

@section('content')
<div class="container mt-6">
    <h1 class="mb-6">Product Create</h1>
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-lg mb-2">Product Details</h2>
            <p class="text-dark-soft">Add your product title, description, and price.</p>
        </div>
        <div class="col-md-12">
            <form action="{{ url("/products") }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <label class="block col-md-6">
                    <span class="block text-medium mb-2">Title</span>
                    <input class="form-control" name="title" placeholder="The World's Best product" value="{{ old('title') }}">
                </label>
                <label class="block col-md-6">
                    <span class="block text-medium mb-2">Description</span>
                    <textarea rows="3" class="form-control" name="description" placeholder="The best product for getting the best information about the best stuff.">{{ old('description') }}</textarea>
                </label>
                <label class="block col-md-6">
                    <span class="block text-medium mb-2">Price</span>
                    <input class="form-control" type="number" name="price" placeholder="5000" value="{{ old('price') }}">
                </label>
                <label class="block col-md-6">
                    <span class="block text-medium mb-2">Active</span>
                    <input class="form-control" type="radio" name="active" value="1" checked>Yes
                    <input class="form-control" type="radio" name="active" value="0">No
                </label>
                <label class="block col-md-6">
                    <span class="block text-medium mb-2">Cover Image</span>
                    <input name="cover_path" type="file" class="form-control-file">
                </label>
                <div class="text-right">
                    <button class="btn btn-primary btn-sm">Create Product</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
