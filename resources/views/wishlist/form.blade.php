<div>
    @if($product->inWishlist(Auth::user()))
        <form action="{{ url("/wishlist/{$product->inWishlist(Auth::user())->id}") }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button class="btn btn-primary" type="submit">Remove from wishlist</button>
        </form>
    @else
        <form action="{{ url("/wishlist") }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="product_id" value="{{ $product->id }}" />
            <button class="btn btn-primary" type="submit">Add to wishlist</button>
        </form>
    @endif
</div>