@extends('layouts.app')

@section('content')
    <div class="bg-circuit mb-6 border-t border-b border-dark-softer">
        <div class="container">
            <div class="py-9">
                <h1 class="text-3xl">My Wishlist</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div>
            <h1 class="text-bold mb-4">Popular Shows</h1>
            <div class="row pull-x-4 pull-b-6">
{{--                {{ dd($wishlist) }}--}}
                @foreach ($wishlists as $wishlist)
                    <div class="col-2 px-4 mb-6">
                        <div class="hover-grow">
                            <a href="{{ url("/products/{$wishlist->product()->id}") }}" class="block box-shadow mb-2">
                                <img src="{{ $wishlist->product()->imageUrl() }}" class="img-fit">
                            </a>
                            <p class="text-ellipsis">
                                <a href="{{ url("/products/{$wishlist->product()->id}") }}" class="text-sm text-medium">
                                    {{ $wishlist->product()->title }}
                                </a>
                            </p>
                            <p class="text-xs text-uppercase text-spaced text-ellipsis">
                                {{ $wishlist->product()->price }} ₴
                            </p>
                        </div>
                        <form action="{{ url("/wishlist/{$wishlist->id}") }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button class="btn btn-primary" type="submit">Remove from wishlist</button>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
