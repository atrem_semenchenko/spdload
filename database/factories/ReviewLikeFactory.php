<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\ReviewLike::class, function (Faker $faker) {
    return [
        'user_id' => factory(App\User::class)->lazy(),
        'review_id' => factory(App\Review::class)->lazy(),
        'like' => true,
    ];
});
