<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Product::class, function (Faker $faker) {
    return [
        'user_id' => factory(App\User::class)->lazy(),
        'title' => 'Product',
        'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        'price' => '5000',
        'cover_path' => 'images/products/product1.png',
    ];
});
