<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Review::class, function (Faker $faker) {
    return [
        'user_id' => factory(App\User::class)->lazy(),
        'product_id' => factory(App\Product::class)->lazy(),
        'text' => "Laboris nisi ut aliquip ex ea commodo consequat."
    ];
});
