<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Wishlist::class, function (Faker $faker) {
    return [
        'user_id' => factory(App\User::class)->lazy(),
        'product_id' => factory(App\Product::class)->lazy(),
    ];
});
