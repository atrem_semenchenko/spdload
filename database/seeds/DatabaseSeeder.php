<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $files = new \Illuminate\Filesystem\Filesystem;
        $files->makeDirectory(storage_path('app/public/images'), 0755, true, true);
        $files->deleteDirectory(storage_path('app/public/images/products'));
        $files->copyDirectory(
            base_path('database/seeds/products/'),
            storage_path('app/public/images/products')
        );

        $user = factory(\App\User::class)->create([
            'email' => 'user@example.com',
            'password' => bcrypt('qweqwe'),
        ]);

        $product1 = factory(\App\Product::class)->create([
            'user_id' => $user->id,
            'title' => 'Bicycle 1',
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            'price' => '6200',
            'cover_path' => 'images/products/prod1.png',
        ]);

        $product2 = factory(\App\Product::class)->create([
            'user_id' => $user->id,
            'title' => 'Bicycle 2',
            'description' => "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
            'price' => '7300',
            'cover_path' => 'images/products/prod2.png',
        ]);

        $product3 = factory(\App\Product::class)->create([
            'user_id' => $user->id,
            'title' => 'Bicycle 3 BMX',
            'description' => "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            'price' => '5500',
            'cover_path' => 'images/products/prod3.png',
        ]);

        $review1 = factory(\App\Review::class)->create([
            'user_id' => $user->id,
            'product_id' => $product1->id,
            'text' => "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ]);

        $review2 = factory(\App\Review::class)->create([
            'user_id' => $user->id,
            'product_id' => $product1->id,
            'text' => "Laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit."
        ]);

        $reviewLike = factory(\App\ReviewLike::class)->create([
            'user_id' => $user->id,
            'review_id' => $review1->id,
            'like' => true,
        ]);

        $reviewLike = factory(\App\ReviewLike::class)->create([
            'user_id' => $user->id,
            'review_id' => $review2->id,
            'like' => false,
        ]);

        $wishlist = factory(\App\Wishlist::class)->create([
            'user_id' => $user->id,
            'product_id' => $product1->id,
        ]);

        $wishlist = factory(\App\Wishlist::class)->create([
            'user_id' => $user->id,
            'product_id' => $product2->id,
        ]);

        $paymentSystem = factory(\App\PaymentSystem::class)->create([
            'name' => "Apple Pay",
        ]);

        $paymentSystem = factory(\App\PaymentSystem::class)->create([
            'name' => "Stripe",
        ]);

        $paymentSystem = factory(\App\PaymentSystem::class)->create([
            'name' => "PayPal",
        ]);


    }
}
