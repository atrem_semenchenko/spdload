<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/products');
});

Route::get('/products',                             'ProductsController@index');
Route::get('/products/{id}',                        'ProductsController@show');

Route::get('/products/{id}/reviews',                'ProductsReviewsController@index');

Route::middleware('auth')->group(function () {
    Route::get('/products/new',                         'ProductsController@create');
    Route::post('/products',                            'ProductsController@store');
    Route::get('/products/{id}/edit',                   'ProductsController@edit');
    Route::patch('/products/{id}',                      'ProductsController@update');
    Route::delete('/products/{id}',                     'ProductsController@destroy');

    Route::get('/order/{id}',                           'OrdersController@create');
    Route::post('/order',                               'OrdersController@store');

    Route::post('/reviews',                             'ReviewsController@store');

    Route::post('/reviews/{id}/like',                   'ReviewLikeController@store');
    Route::patch('/reviews/like/{id}',                  'ReviewLikeController@update');
    Route::delete('/reviews/like/{id}',                 'ReviewLikeController@destroy');

    Route::get('/wishlist/my',                          'WishlistController@index');
    Route::post('/wishlist',                            'WishlistController@store');
    Route::delete('/wishlist/{id}',                     'WishlistController@destroy');

    Route::put('/products/{id}/cover-image',            'ProductsCoverImageController@update');
});

Auth::routes();
