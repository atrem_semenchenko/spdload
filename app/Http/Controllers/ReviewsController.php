<?php

namespace App\Http\Controllers;

use App\Product;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewsController extends Controller
{
    public function store()
    {
        request()->validate([
            'text' => ['required', 'max:500'],
        ]);

        $product = Product::active()->findOrFail(request('product_id'));

        $review = Review::create([
            'user_id' => Auth::user()->id,
            'product_id' => $product->id,
            'text' => request()->get('text'),
        ]);

        return redirect()->back();
    }
}
