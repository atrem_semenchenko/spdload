<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::active()->paginate(10);

        return view('products.index', [
            'products' => $products,
        ]);
    }

    public function create()
    {
        return view('products.create');
    }

    public function store()
    {
        request()->validate([
            'title' => ['required', 'max:150'],
            'description' => ['max:500'],
            'price' => ['required'],
            'cover_path' => ['required', 'image']
        ]);

        $coverPath = request()->file('cover_path')->store('images/products', 'public');

        $product = Auth::user()->products()->create([
            'title' => request()->get('title'),
            'description' => request()->get('description'),
            'price' => request()->get('price'),
            'active' => request()->get('active'),
            'cover_path' => $coverPath,
        ]);

        return redirect("/products/{$product->id}");
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);

        return view('products.show', [
            'product' => $product,
            'reviews' => $product->lastReviews(2),
        ]);
    }

    public function edit($id)
    {
        $product = Auth::user()->products()->findOrFail($id);

        return view('products.edit', [
            'product' => $product,
        ]);
    }

    public function update($id)
    {
        $product = Auth::user()->products()->findOrFail($id);

        request()->validate([
            'title' => ['required', 'max:150'],
            'description' => ['max:500'],
            'price' => ['required'],
        ]);

        $product->update(request([
            'title',
            'description',
            'price',
            'active',
        ]));

        return redirect("/products/{$product->id}");
    }

    public function destroy($id)
    {
        $product = Auth::user()->products()->findOrFail($id);

        $product->delete();

        return redirect("/products");
    }
}
