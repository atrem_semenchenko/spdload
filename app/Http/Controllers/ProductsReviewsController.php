<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsReviewsController extends Controller
{
    public function index($id)
    {
        $product = Product::findOrFail($id);

        return view('products-reviews.index', [
            'product' => $product,
            'reviews' => $product->reviews(),
        ]);
    }
}
