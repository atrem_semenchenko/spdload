<?php

namespace App\Http\Controllers;

use App\Review;
use App\ReviewLike;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewLikeController extends Controller
{
    public function store($id)
    {
        $review = Review::findOrFail($id);

        $reviewLike = ReviewLike::create([
            'user_id' => Auth::user()->id,
            'review_id' => $id,
            'like' => request()->get("like"),
        ]);

        return redirect()->back();
    }

    public function update($id)
    {
        $reviewLike = Auth::user()->reviewLike()->findOrFail($id);

        $reviewLike->update([
            'like' => !$reviewLike->like,
        ]);

        return redirect()->back();
    }

    public function destroy($id)
    {
        $reviewLike = Auth::user()->reviewLike()->findOrFail($id);

        $reviewLike->delete();

        return redirect()->back();
    }
}
