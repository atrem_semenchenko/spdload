<?php

namespace App\Http\Controllers;

use App\Order;
use App\PaymentSystem;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    public function create($id)
    {
        $product = Product::active()->findOrFail($id);
        $paymentSystems = PaymentSystem::all();

        return view('orders.create', [
            'product' => $product,
            'paymentSystems' => $paymentSystems
        ]);
    }

    public function store()
    {
        $product = Product::active()->findOrFail(request()->get('product_id'));
        $paymentSystem = PaymentSystem::findOrFail(request()->get('payment_system'));

        $order = Order::create([
            'user_id' => Auth::user()->id,
            'product_id' => $product->id,
            'payment_system_id' => $paymentSystem->id,
            'total' => $product->price,
        ]);

        return redirect()->back();
    }
}
