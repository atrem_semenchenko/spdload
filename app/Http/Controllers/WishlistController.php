<?php

namespace App\Http\Controllers;

use App\Product;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    public function index()
    {
        $wishlists = Auth::user()->wishlist()->get();

        return view('wishlist.index', [
            'wishlists' => $wishlists,
        ]);
    }

    public function store()
    {
        $product = Product::active()->findOrFail(request()->get('product_id'));

        $wishlist = Wishlist::create([
            'user_id' => Auth::user()->id,
            'product_id' => $product->id,
        ]);

        return redirect()->back();
    }

    public function destroy($id)
    {
        $wishlist = Auth::user()->wishlist()->findOrFail($id);

        $wishlist->delete();

        return redirect()->back();
    }
}
