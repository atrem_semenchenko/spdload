<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductsCoverImageController extends Controller
{
    public function update($id)
    {
        $product = Auth::user()->products()->findOrFail($id);

        request()->validate([
            'cover_path' => ['required', 'image'],
        ]);

        $product->update([
            'cover_path' => request()->file('cover_path')->store('images/products', 'public'),
        ]);

        return redirect("/products/{$product->id}");
    }
}
