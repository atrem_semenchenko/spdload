<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    protected $fillable = [
        'user_id', 'title', 'description', 'price', 'active', 'cover_path'
    ];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function wishlist()
    {
        return $this->hasMany(Wishlist::class);
    }

    public function imageUrl()
    {
        return Storage::disk('public')->url($this->cover_path);
    }

    public function lastReviews($count = 2)
    {
        return $this->reviews()->latest()->take($count)->get();
    }

    public function isOwnedBy($user)
    {
        return $user ? $this->user_id == $user->getKey() : false;
    }

    public function inWishlist($user)
    {
        return $this->wishlist()->where('user_id', $user->id)->first();
    }
}
